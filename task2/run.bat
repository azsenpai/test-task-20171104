@ECHO OFF

IF [%1] == [] (
    SET M=3
) ELSE (
    SET M=%1
)

IF [%2] == [] (
    SET N=100
) ELSE (
    SET N=%2
)

FOR /L %%i IN (1, 1, %M%) DO (
    start main.bat %N%
)

SET /A RESULT=N*M

ECHO RESULT MUST BE: %RESULT%
