@ECHO OFF

IF [%1] == [] (
    SET N=100
) ELSE (
    SET N=%1
)

FOR /L %%i IN (1, 1, %N%) DO (
    echo %%i
    php main.php
)
