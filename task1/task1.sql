-- http://sqlfiddle.com/#!9/483d74/1

CREATE TABLE `books` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;

INSERT INTO `books` (`name`) VALUES ('Алгоритмы: построение и анализ');
INSERT INTO `books` (`name`) VALUES ('Искусство программирования');

CREATE TABLE `authors` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;

INSERT INTO `authors` (`name`) VALUES ('Кормен, Томас');
INSERT INTO `authors` (`name`) VALUES ('Лейзерсон, Чарльз Эрик');
INSERT INTO `authors` (`name`) VALUES ('Ривест, Рональд Линн');
INSERT INTO `authors` (`name`) VALUES ('Кнут, Дональд Эрвин');

CREATE TABLE `book_authors` (
	`book_id` INT(10) UNSIGNED NULL DEFAULT NULL,
	`author_id` INT(10) UNSIGNED NULL DEFAULT NULL,
	UNIQUE INDEX `book_id_author_id` (`book_id`, `author_id`),
	INDEX `book_id` (`book_id`),
	INDEX `author_id` (`author_id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;

INSERT INTO `book_authors` (`book_id`, `author_id`) VALUES ('1', '1');
INSERT INTO `book_authors` (`book_id`, `author_id`) VALUES ('1', '2');
INSERT INTO `book_authors` (`book_id`, `author_id`) VALUES ('1', '3');
INSERT INTO `book_authors` (`book_id`, `author_id`) VALUES ('2', '4');

SELECT b.name
FROM books b
WHERE b.id IN (
	SELECT ba.book_id
	FROM book_authors ba
	GROUP BY ba.book_id
	HAVING COUNT(ba.book_id) = 3
)
