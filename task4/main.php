<?php

require('BigInt.php');

$s1 = fgets(STDIN);
$s2 = fgets(STDIN);

$a = new BigInt($s1);
$b = new BigInt($s2);

$c = $a->add($b);

print($c);
