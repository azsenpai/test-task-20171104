<?php

/**
 * @author <zh.adlet@gmail.com>
 */
class BigInt
{
    private $len;
    private $digits;
    private $base;
    private $sign;

    /**
     * @param $s
     * @param $base
     */
    public function __construct($s = '0', $base = 10)
    {
        $s = trim($s);
        $s = empty($s) ? '0' : $s;

        $this->sign = $s[0] == '-' ? -1 : 1;

        while (strlen($s) > 0 && !is_numeric($s[0])) {
            $s = substr($s, 1);
        }

        $l = strlen($s);
        $d = [];

        for ($i = 0; $i < $l; $i ++) {
            $d[$i] = intval($s[$l-$i-1]);
        }

        $this->len = $l;
        $this->digits = $d;
        $this->base = $base;
    }

    /**
     * @inherit
     */
    public function __toString()
    {
        $l = $this->len;
        $d = $this->digits;
        $s = $this->sign < 0 ? '-' : '';

        for ($i = 0; $i < $l; $i ++) {
            $s .= strval($d[$l-$i-1]);
        }

        return $s;
    }

    /**
     *
     */
    public function inv()
    {
        $result = clone $this;
        $result->sign *= -1;

        return $result;
    }

    /**
     * @param BigInt $right
     */
    public function cmp(BigInt $right)
    {
        if ($this->sign > 0 && $right->sign < 0) {
            return 1;
        }

        if ($this->sign < 0 && $right->sign > 0) {
            return -1;
        }

        $ll = $this->len;
        $ld = $this->digits;

        $rl = $right->len;
        $rd = $right->digits;

        if ($this->sign > 0 && $right->sign > 0) {
            if ($ll > $rl) {
                return 1;
            } else if ($ll < $rl) {
                return -1;
            }

            for ($i = 0; $i < $ll; $i ++) {
                if ($ld[$i] > $rd[$i]) {
                    return 1;
                } else if ($ld[$i] < $rd[$i]) {
                    return -1;
                }
            }

            return 0;
        }

        if ($ll > $rl) {
            return -1;
        } else if ($ll < $rl) {
            return 1;
        }

        for ($i = 0; $i < $ll; $i ++) {
            if ($ld[$i] > $rd[$i]) {
                return -1;
            } else if ($ld[$i] < $rd[$i]) {
                return 1;
            }
        }

        return 0;
    }

    /**
     * @param BigInt $right
     */
    public function add(BigInt $right)
    {
        if ($this->sign > 0 && $right->sign < 0) {
            $result = $this->sub($right->inv());

            return $result;
        }

        if ($this->sign < 0 && $right->sign > 0) {
            $result = $right->sub($this->inv());

            return $result;
        }

        $sign = ($this->sign < 0 && $right->sign < 0) ? -1 : 1;
        $base = $this->base;

        $ll = $this->len;
        $ld = $this->digits;

        $rl = $right->len;
        $rd = $right->digits;

        $l = max($ll, $rl);
        $d = [];
        $carry = 0;

        while ($ll < $l) {
            $ld[$ll] = 0;
            $ll ++;
        }

        while ($rl < $l) {
            $rd[$rl] = 0;
            $rl ++;
        }

        for ($i = 0; $i < $l; $i ++) {
            $d[$i] = $ld[$i] + $rd[$i] + $carry;
            $carry = intval($d[$i] / $base);
            $d[$i] %= $base;
        }

        while ($carry > 0) {
            $d[$l] = $carry % $base;
            $l += 1;
            $carry = intval($carry / $base);
        }

        $result = new BigInt();

        $result->len = $l;
        $result->digits = $d;
        $result->base = $base;
        $result->sign = $sign;

        return $result;
    }

    /**
     * @param BigInt $right
     */
    public function sub(BigInt $right)
    {
        if ($this->sign > 0 && $right->sign > 0) {
            if ($this->cmp($right) < 0) {
                $result = $right->sub($this);
                $result->sign = -1;

                return $result;
            }
        }

        if ($this->sign > 0 && $right->sign < 0) {
            $result = $this->add($right->inv());

            return $result;
        }

        if ($this->sign < 0 && $right->sign > 0) {
            $result = $this->inv()->add($right);
            $result->sign = -1;

            return $result;
        }

        if ($this->sign < 0 && $right->sign < 0) {
            $result = $right->inv()->add($this->inv());

            return $result;
        }

        $sign = 1;
        $base = $this->base;

        $ll = $this->len;
        $ld = $this->digits;

        $rl = $right->len;
        $rd = $right->digits;

        $l = max($ll, $rl);
        $d = [];

        while ($ll < $l) {
            $ld[$ll] = 0;
            $ll ++;
        }

        while ($rl < $l) {
            $rd[$rl] = 0;
            $rl ++;
        }

        for ($i = 0; $i < $l; $i ++) {
            $d[$i] = $ld[$i] - $rd[$i];

            if ($d[$i] < 0) {
                $d[$i] += $base;
                $ld[$i+1] -= 1;
            }
        }

        while ($l > 0 && $d[$l-1] == 0) {
            $l -= 1;
        }

        $result = new BigInt();

        $result->len = $l;
        $result->digits = $d;
        $result->base = $base;
        $result->sign = $sign;

        return $result;
    }
}
