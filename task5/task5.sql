-- http://sqlfiddle.com/#!9/039a1/173

CREATE TABLE test (
  id INT NOT NULL PRIMARY KEY
);

INSERT INTO test (id) VALUES (1), (2), (3), (6), (8), (9), (12);


SET @row_number1 = 0;
SET @row_number2 = 0;

SELECT T1.id `FROM`, T2.id `TO`
FROM (
  SELECT id, (@row_number1 := @row_number1 + 1) row_number
  FROM test
  ORDER BY id
) T1, (
  SELECT id, (@row_number2 := @row_number2 + 1) row_number
  FROM test
  ORDER BY id
) T2
WHERE T2.row_number - T1.row_number = 1 AND T2.id - T1.id > 1

